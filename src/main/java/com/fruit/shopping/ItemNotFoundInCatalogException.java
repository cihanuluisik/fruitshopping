package com.fruit.shopping;

public class ItemNotFoundInCatalogException extends RuntimeException {
    public ItemNotFoundInCatalogException(String message) {
        super(message);
    }
}
