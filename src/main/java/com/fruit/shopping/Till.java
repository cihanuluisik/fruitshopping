package com.fruit.shopping;

import com.fruit.calculator.Calculator;
import com.fruit.catalog.Catalog;
import com.fruit.catalog.Fruit;

import java.math.BigDecimal;
import java.util.List;

public class Till {

    private final Catalog catalog;
    private final Calculator calculator;

    public Till(Catalog catalog) {
        this.catalog = catalog;
        calculator = new Calculator();
    }

    public BigDecimal calculate(String[] itemNames) {

        final List<Fruit> fruits = catalog.validateItemNames(itemNames);

        // total calculation
        final BigDecimal total = calculator.calculateTotal(fruits);

        // discount calculation
        final BigDecimal discountTotal = calculator.calculateDiscount(fruits);

        return total.add(discountTotal);
    }




}
