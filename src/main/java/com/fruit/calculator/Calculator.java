package com.fruit.calculator;

import com.fruit.catalog.Fruit;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Calculator {

    public BigDecimal calculateTotal(List<Fruit> fruits) {
        return fruits.stream().map(Fruit::getPrice).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
    }
    public BigDecimal calculateDiscount(List<Fruit> fruits) {
        final Map<Fruit, Long> groupedByItems = fruits.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return  groupedByItems.entrySet().stream()
                .map(me -> calculateItemDiscount(me.getKey(), me.getValue()))
                .reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
    }

    private BigDecimal calculateItemDiscount(Fruit fruit, Long number) {
        return fruit.getDiscount().multiply((new BigDecimal(number / fruit.getDiscountThreshold())));
    }

}
