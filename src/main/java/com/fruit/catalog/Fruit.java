package com.fruit.catalog;

import java.math.BigDecimal;

public class Fruit {
    private final String name;
    private BigDecimal price;
    private BigDecimal discount;
    private int discountThreshold;

    public Fruit(String name, BigDecimal price, BigDecimal discount, int discountThreshold) {
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.discountThreshold = discountThreshold;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public int getDiscountThreshold() {
        return discountThreshold;
    }
}
