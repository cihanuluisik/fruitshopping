package com.fruit.catalog;

import com.fruit.shopping.ItemNotFoundInCatalogException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Catalog {

    public static final String ERR_MSG_MUST_BE_GIVEN_A_BASKET = "Must be given a basket!";
    public static final String ERR_MSG_ITEM_IS_NOT_FOUND_IN_THE_CATALOG = "Item is not found in the catalog : ";
    private final Map<String, Fruit> catalogItems = new HashMap<>();

    public Catalog() {
        catalogItems.put("apple", new Fruit("Orange", new BigDecimal("0.60"), new BigDecimal("-0.60"), 2));
        catalogItems.put("orange", new Fruit("Orange", new BigDecimal("0.25"), new BigDecimal("-0.25"), 3));
    }


    private String convertItemNameToCatalogStandard(String itemName) {
        return itemName.toLowerCase().trim();
    }


    private Fruit getFruitFromCatalog(String itemName) {
        final Fruit fruit = catalogItems.get(convertItemNameToCatalogStandard(itemName));
        if( fruit==null) {
            throw new ItemNotFoundInCatalogException(ERR_MSG_ITEM_IS_NOT_FOUND_IN_THE_CATALOG + itemName);
        }
        return fruit;
    }


    public List<Fruit> validateItemNames(String[] itemNames) {
        if ( itemNames == null){
            throw new IllegalArgumentException(ERR_MSG_MUST_BE_GIVEN_A_BASKET);
        }
        return Arrays.stream(itemNames).map(itemName -> getFruitFromCatalog(itemName)).collect(Collectors.toList());
    }
}
