package com.fruit.shopping;

import com.fruit.catalog.Catalog;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TillTest {

    private final Till till = new Till(new Catalog());

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void should_throw_error_given_no_basket() throws Exception {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(Catalog.ERR_MSG_MUST_BE_GIVEN_A_BASKET);
        till.calculate(null);
    }

    @Test
    public void should_return_zero_if_given_empty_basket() throws Exception {
        assertThat(till.calculate(new String[]{}), is(BigDecimal.ZERO));
    }

    @Test
    public void should_throw_error_given_basket_with_unknown_items() throws Exception {
        expectedEx.expect(ItemNotFoundInCatalogException.class);
        final String unknowItemName = "unknownItem";
        expectedEx.expectMessage(Catalog.ERR_MSG_ITEM_IS_NOT_FOUND_IN_THE_CATALOG + unknowItemName);
        till.calculate(new String[]{unknowItemName});
    }

    @Test
    public void should_calculate_correct_given_single_item_baskets() throws Exception {
        assertThat(till.calculate(new String[]{"Apple"}),  is(new BigDecimal("0.60")));
        assertThat(till.calculate(new String[]{"Orange"}), is(new BigDecimal("0.25")));
    }

    @Test
    public void should_calculate_correct_given_item_name_case_insensitive() throws Exception {
        assertThat(till.calculate(new String[]{"ApPle"}),  is(new BigDecimal("0.60")));
        assertThat(till.calculate(new String[]{"ORANGE"}), is(new BigDecimal("0.25")));
    }

    @Test
    public void should_calculate_correct_given_item_name_have_trailing_spaces() throws Exception {
        assertThat(till.calculate(new String[]{"Apple "}), is(new BigDecimal("0.60")));
        assertThat(till.calculate(new String[]{" Orange "}), is(new BigDecimal("0.25")));
    }

    @Test
    public void should_calculate_correct_given_items_of_same_type_and_discount_matching_number_in_the_basket() throws Exception {
        assertThat(till.calculate(new String[]{"Apple", "Apple"}),              is(new BigDecimal("0.60")));
        assertThat(till.calculate(new String[]{"Orange", "Orange", "Orange"}),  is(new BigDecimal("0.50")));
    }

}
