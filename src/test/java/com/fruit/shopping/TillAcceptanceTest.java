package com.fruit.shopping;

import com.fruit.catalog.Catalog;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TillAcceptanceTest {

    private final Till till = new Till(new Catalog());

    @Test
    public void should_support_basket_pricing() throws Exception {
        assertThat(till.calculate(new String[]{"Apple",  "Orange"}),
                is(new BigDecimal("0.85")));
    }


    @Test
    public void should_support_basket_pricing_with_discount() throws Exception {
        assertThat(till.calculate(new String[]{"Apple","Apple","Apple", "Orange", "Orange", "Orange", "Orange"}),
                is(new BigDecimal("1.95")));
    }


}